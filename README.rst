the yaml file is modified from what one gets with


.. code:: bash

  esphome curious.yaml wizard


changes are mainly hiding usernames and passwords

added simple i2c config for bme280, mqtt and the sensor itself

to compile

.. code:: bash

  esphome curious.yaml compile

to clean

.. code:: bash

  esphome curious.yaml clean

to upload

.. code:: bash

  esphome curious.yaml upload

to debug

.. code:: bash

  esphome curious.yaml run

once the initial upload is done everything else can ve done OTA
